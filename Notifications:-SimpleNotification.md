A notification that slides in and out from either the top or the bottom of the screen, consisting of some text and optionally, a glyph alongside it.

![](https://i.imgur.com/qtDp8yN.png)

## Properties

-	`Text`  
	The text to be displayed.

-	`Action`  
	The action to be executed upon invocation of the notification.

-	`Glyph`  
	The glyph to be displayed alongside the text.

-	`GlyphFontFamily`  
	The font family for the glyph.

-	`PullAwayLength`  
	The length that a notification can be pulled away from its edge of the screen.

-	`HideThreshold`  
	The length that the notification can be pulled towards its edge of the screen, exceeding which, it will be hidden, otherwise, restored.

-	`TransitionDuration`  
	The duration of the sliding transitions used to enter and exit the viewport.

-	`CompactWidth`  
	How wide the notification is when the viewport is wide (when it ideally shouldn't stretch horizontally).  
	Set this to `Double.NaN` if the notification should stretch horizontally, regardless of the width of the viewport.

-	`Breakpoint`  
	The width that, when exceeded, the notification becomes 'compact', for large viewport widths.

## Vertical Alignment

The notification can be aligned to either the top or the bottom using the `VerticalAlignment` property.

![](https://i.imgur.com/SKT0NoD.png)