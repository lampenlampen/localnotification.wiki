![Demonstrative application](https://github.com/RavinduL/LocalNotifications/raw/master/images/demo.gif)

An application that demonstrates some of the features of the library is included in the repository, whose project is located at `src/RavinduL.LocalNotifications.Demo/RavinduL.LocalNotifications.Demo.csproj`. Click [here](https://github.com/RavinduL/LocalNotifications/tree/master/src/RavinduL.LocalNotifications.Demo) to browse its source.

## Running the Application

1. Open the Visual Studio solution located at `src/RavinduL.LocalNotifications.sln`.

2. Set the `RavinduL.LocalNotifications.Demo` project as the startup project via the Solution Explorer.

3.	Ensure that the 'Build' and 'Deploy' options are checked for both projects via the Configuration Manager.
	
	![Configuration Manager](https://i.imgur.com/ok8loTk.png)

4. Run the application using <kbd>Ctrl</kbd> + <kbd>F5</kbd>.
