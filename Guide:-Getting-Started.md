This library enables display and management of in-app notifications in Universal Windows projects.

Unlike [toast notifications](https://docs.microsoft.com/en-us/windows/uwp/controls-and-patterns/tiles-badges-notifications) that are managed by Windows, local notifications are managed by your application, and therefore will not appear in the Action Center.

## Installation

The suggested method of installing the library is to obtain it via [NuGet](https://www.nuget.org) by searching for [the package](https://www.nuget.org/packages/RavinduL.LocalNotifications) in the NuGet gallery or typing the following command in the Package Manager Console within Visual Studio,

> `Install-Package RavinduL.LocalNotifications`

Alternatively, you could clone [the repository](https://github.com/RavinduL/LocalNotifications) and include the project located at `src/RavinduL.LocalNotifications/RavinduL.LocalNotifications.csproj` project from within yours, or build the aforementioned project and reference the assembly. 

## Usage

1.	**Create a Grid to be assigned to a `LocalNotificationManager`.**

	```XML
	<Grid x:Name="NotificationGrid" />
	```

	The grid should ideally be sized to fill the entire screen. In order to ensure that notifications appear above other elements on the page, define the grid towards the bottom of the XAML file (thereby positioning it above the elements defined physically above it in the markup). For example,

	```XML
	<Page ...>
		<Grid Background="{ThemeResource ApplicationPageBackgroundThemeBrush}">
			<!-- Elements *overlapped by* NotificationGrid (and thereby, local notifications) go above this comment -->
			<Grid x:Name="NotificationGrid" />
		</Grid>
	</Page>
	```

	Make sure not to assign it a `Background` (not even 'Transparent') to ensure interactivity with elements that it overlaps.

2.	**Create a `LocalNotificationManager` and assign the newly created Grid to it.**

	In the code-behind of the page, create a new instance of the `LocalNotificationManager` class, passing the newly created grid to its constructor.

	```C#
	using RavinduL.LocalNotifications;
	using RavinduL.LocalNotifications.Notifications;

	public sealed partial class MainPage : Page
	{
		private LocalNotificationManager manager;

		public MainPage()
		{
			InitializeComponent();

			// This event can be subscribed to via XAML, as <Page ... Loaded="MainPage_Loaded" ...
			Loaded += MainPage_Loaded;
		}

		private void MainPage_Loaded(object sender, RoutedEventArgs e)
		{
			manager = new LocalNotificationManager(NotificationGrid);
		}
	}
	```

3.	**Display a local notification via the manager.**

	Create a local notification by instantiating a class within the `RavinduL.LocalNotifications.Notifications` namespace (in this case, the [`SimpleNotification`](https://github.com/RavinduL/LocalNotifications/wiki/Notifications:-SimpleNotification)), show it using the `LocalNotificationManager.Show` instance method.

	```C#
	manager.Show(new SimpleNotification
	{
		TimeSpan = TimeSpan.FromSeconds(3),
		Text = "Notification text",
		Glyph = "\uE006",
		Action = async () => await new MessageDialog("Notification tapped.").ShowAsync(),
	});
	```